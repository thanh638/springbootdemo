package vn.thanhtv.springbootdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//@RequestMapping(value="/student")
public class StudentController {

	@RequestMapping(value="/")
	public String index() {
		return "index";
	}
}
